import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dishant Mishra
 * @version 0.5
 * <h1>Purpose</h1>
 * <p>
 *  This program loads a text file into a hash table, character by character, to be
 *  used by intellisense or autocorrect.
 * </p>
 * @TODO Thread the application
 * @TODO Optimizations
 * @TODO Eliminate unchecked exceptions
 */

public class model extends Thread {
    private ConcurrentHashMap<Character, ConcurrentHashMap<Character, ConcurrentHashMap>> root = null;
    private final String INPUTFILE = "words_alpha.txt";

    private ConcurrentHashMap<Character, ConcurrentHashMap<Character, ConcurrentHashMap>> loadFile(File file) {
        ConcurrentHashMap<Character, ConcurrentHashMap<Character, ConcurrentHashMap>> internal = null;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            internal = new ConcurrentHashMap<>();
            while( (line=br.readLine()) != null ){
                line = line.trim();
                if(!line.equals("")) {
                    char first = line.charAt(0);
                    String later = line.substring(1);
                    if(Character.isLetter(first)) {
                        if(internal.containsKey(first)) {
                            seeker(later, internal.get(first));
                        } else {
                            internal.put(first, new ConcurrentHashMap<>());
                            seeker(later, internal.get(first));
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return internal;
    }

    private ConcurrentHashMap seeker(String word, ConcurrentHashMap<Character, ConcurrentHashMap> old) {
        ConcurrentHashMap internal = null;
        if(!Objects.equals(word, "")) {
            char first = word.charAt(0);
            String after = word.substring(1);
            if (old.containsKey(first)) {
                internal = seeker(after, old.get(first));
            } else {
                old.put(first, new ConcurrentHashMap<>());
                internal = seeker(after, old.get(first));
            }
        }

        return internal;
    }

    @Override
    public void run() {
        model tnis = new model();
        tnis.root = tnis.loadFile(new File(tnis.INPUTFILE));
    }
}
